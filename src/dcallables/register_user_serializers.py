# register user serializers

EXT_TYPE_USER = 0x41
TYPEID_PANDAS = bytes([EXT_TYPE_USER,ord('p')])
TYPEID_NUMPY = bytes([EXT_TYPE_USER,ord('n')])
TYPEID_PICKLE = bytes([EXT_TYPE_USER,ord('P')])

def register():
    from dagger4.msgpack import register_serializer, register_deserializer

    def _np_pred(obj):
        import numpy as np
        return type(obj) is np.ndarray
    def _np_serializer(buffer, obj, ser):
        import numpy as np
        assert type(obj) is np.ndarray
        buffer.write(obj.tostring())
    def _np_deserializer(buffer, buff_i, n, deser):
        import numpy as np
        return np.frombuffer(buffer[buff_i:buff_i+n])
    register_serializer(0, TYPEID_NUMPY, _np_pred, _np_serializer)
    register_deserializer(TYPEID_NUMPY, _np_deserializer)
    
    def _pd_pred(obj):
        import pandas as pd
        return type(obj) is pd.DataFrame
    def _pd_serializer(buffer, obj, ser):
        import pandas as pd
        assert type(obj) is pd.DataFrame
        obj.to_pickle(buffer, compression=None)
    def _pd_deserializer(buffer, buff_i, n, deser):
        from io import BytesIO
        import pandas as pd
        buf = BytesIO(buffer[buff_i:buff_i+n])
        r = pd.read_pickle(buf, compression=None)
        buf.close()
        return r
    register_serializer(0, TYPEID_PANDAS, _pd_pred, _pd_serializer)
    register_deserializer(TYPEID_PANDAS, _pd_deserializer)

    def _pickle_pred(obj):
        import requests
        return type(obj) is requests.models.Response
    def _pickle_serializer(buffer, obj, ser):
        import requests
        assert type(obj) is requests.models.Response
        import pickle
        pickle.dump(obj, buffer)
    def _pickle_deserializer(buffer, buff_i, n, deser):
        from io import BytesIO
        import pickle
        buf = BytesIO(buffer[buff_i:buff_i+n])
        r = pickle.load(buf)
        buf.close()
        return r
    import sys
    register_serializer(sys.float_info.max, TYPEID_PICKLE, _pickle_pred, _pickle_serializer)
    register_deserializer(TYPEID_PICKLE, _pickle_deserializer)
