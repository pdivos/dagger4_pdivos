from dagger4 import DFunction, DLambda, call, Fiddle, ts, DepCollector
import json

@DFunction
def test_tor():
    import requests
    r = requests.get(
            'https://check.torproject.org/',
            proxies={'https':'socks5://127.0.0.1:9050'},
            timeout=10,
        )
    assert r.status_code == 200
    assert b'Congratulations. This browser is configured to use Tor.' in r.content
    return True

@DFunction
def opta_get(url):
    import requests
    headers = {
        "Connection": "keep-alive",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/62.0.3202.94 Chrome/62.0.3202.94 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
    }
    r = requests.get(url, headers=headers, proxies={'https':'socks5://127.0.0.1:9050','http':'socks5://127.0.0.1:9050'})
    return r

@DLambda
def opta_decode_feed_loads(feed_id, data):
    # opta_decode_feed is in dagger4_pdivos_java dcommit
    # returns json string which is decoded here
    ret = call("opta_decode_feed", feed_id, data)
    return json.loads(ret)

_opta_base_url = "http://omo.akamai.opta.net/"
_opta_user_psw = "&user=OW2017&psw=dXWg5gVZ"
@DFunction
def opta_get_f24(game_id):
    url = _opta_base_url + "?feed_type=f24_packed&game_id=" + str(game_id) + _opta_user_psw
    r = call('opta_get',url)
    assert r.status_code == 200, r.status_code
    resp = r.content
    data = json.loads(resp.decode("utf-8"))
    assert type(data) is dict, str(type(data)) + "," + str(data)
    assert 'data' in data, data.keys()
    return call("opta_decode_feed_loads", 'f24', data['data'])

@DFunction
def opta_get_f9(game_id):
    url = _opta_base_url + "?feed_type=f9_packed&game_id=" + str(game_id) + _opta_user_psw
    r = call('opta_get',url)
    assert r.status_code == 200, r.status_code
    resp = r.content
    data = json.loads(resp.decode("utf-8"))
    assert type(data) is dict, str(type(data)) + "," + str(data)
    assert 'data' in data, data.keys()
    return call("opta_decode_feed_loads", 'f9', data['data'])

@DFunction
def opta_get_f1(season_id, competition_id):
    url = _opta_base_url + "competition.php?feed_type=f1_packed&competition=" + str(competition_id) + "&season_id=" + str(season_id) + _opta_user_psw
    r = call('opta_get',url)
    assert r.status_code == 200, r.status_code
    resp = r.content
    data = json.loads(resp.decode("utf-8"))
    assert type(data) is dict, str(type(data)) + "," + str(data)
    assert 'data' in data, data.keys()
    return call("opta_decode_feed_loads", 'f1', data['data'])

def _pvs_decode(resp):
    # decodes resp from: http://widget.cloud.opta.net/translations_v2/default/CP_default_1_en_GB_1_2018.json
    start = resp.find('(')
    assert start > 0
    resp = resp[start+1:-1]
    resp = json.loads(resp)
    assert type(resp) is dict and 'd' in resp
    rows = resp['d'].split('¦')
    resp = []
    for r in rows:
        r = r.split('|')
        if len(r) != 4: continue
        resp.append({
            'id': int(r[0]),
            'name': r[1],
            'shortname': r[2],
            'abbr': r[3]
        })
    return resp

@DFunction
def opta_get_competition_ids(season_id):
    feed_id = "CP" + "_default_1_en_GB_1_" + str(season_id)
    url = "http://widget.cloud.opta.net/translations_v2/default/" + feed_id + ".json"
    r = call('opta_get',url)
    assert r.status_code == 200, r.status_code
    resp = r.content.decode('utf-8')
    return _pvs_decode(resp)

@DFunction
def opta_get_team_ids(season_id, competition_id):
    feed_id = "TN" + "_default_1_en_GB_1_" + str(season_id) + "_" + str(competition_id)
    url = "http://widget.cloud.opta.net/translations_v2/default/" + feed_id + ".json"
    r = call('opta_get',url)
    assert r.status_code == 200, r.status_code
    resp = r.content.decode('utf-8')
    return _pvs_decode(resp)

@DFunction
def opta_get_player_ids(season_id, competition_id, team_id):
    feed_id = "PN" + "_default_1_en_GB_1_" + str(season_id) + "_" + str(competition_id) + "_" + str(team_id)
    url = "http://widget.cloud.opta.net/translations_v2/default/" + feed_id + ".json"
    r = call('opta_get',url)
    assert r.status_code == 200, r.status_code
    resp = r.content.decode('utf-8')
    return _pvs_decode(resp)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# higher level functions

import pandas as pd

@DFunction
def opta_parser_get_competitions_current():
    """
    rerurns competitions for this year and the previous
    """
    from utils.time import to_str
    year = int(to_str(ts())[:4])
    df_competitions = pd.DataFrame()
    for year in [year-1, year]:
        df_competitions_y = pd.DataFrame(call('opta_get_competition_ids', year))
        df_competitions_y['year'] = year
        df_competitions = df_competitions.append(df_competitions_y)
    df_competitions.reset_index(inplace=True)
    return df_competitions

@DFunction
def opta_parser_f1_df(season_id, competition_id):
    f1 = call('opta_get_f1', season_id, competition_id)
    teams = call('opta_get_team_ids', season_id, competition_id)

    df_f1 = []
    df_teams = []

    if 'error' in f1:
        if f1['error']['code'] == -32000 and 'Error: competition, feed_type, season_id combination not found in the feed repository' in f1['error']['message']:
            raise Exception(f1['error']['message'])
        else:
            assert False, 'Unknown error: ' + str(f1)
    if type(teams) is dict and 'error' in teams:
        if teams['error']['code'] == 403:
            raise Exception(str(teams))
        else:
            assert False, 'Unknown error: ' + str(teams)

    assert 'data' in f1, str(f1.keys())
    f1 = f1['data']['OptaFeed']['OptaDocument']
    assert season_id == f1['@attributes']['season_id']
    assert competition_id == f1['@attributes']['competition_id']

    [t.update({'season_id':season_id}) for t in teams]
    [t.update({'competition_id':competition_id}) for t in teams]
    df_teams += teams

    matchdatas = f1['MatchData']
    for matchdata in matchdatas:
        game_id = matchdata['@attributes']['uID']
        assert game_id[0]=='g'
        date = matchdata['MatchInfo']['dateObj']['utc']
        period = matchdata['MatchInfo']['@attributes']['Period']
        assert period in ['PreMatch', 'FullTime', 'Abandoned', 'Live', 'Postponed'], period
        teams = {
            matchdata['TeamData'][0]['@attributes']['Side']:matchdata['TeamData'][0]['@attributes']['TeamRef'],
            matchdata['TeamData'][1]['@attributes']['Side']:matchdata['TeamData'][1]['@attributes']['TeamRef'],
        }
        assert list(teams.values())[0][0]=='t'
        assert list(teams.values())[1][0]=='t'
        row = {
            'season_id': season_id,
            'competition_id': competition_id,
            'game_id': game_id,
            'datetime': date,
            'period': period,
            'team_home': teams['Home'],
            'team_away': teams['Away'],
        }
        df_f1.append(row)

    df_f1 = pd.DataFrame(df_f1)
    df_teams = pd.DataFrame(df_teams)
    teams_dict = {}
    for ids, row in df_teams.iterrows():                                                                    
        teams_dict[int(row['id'])] = row['name']
    for k in ['home','away']:
        df_f1['team_name_'+k] = df_f1['team_'+k].apply(lambda v, teams_dict=teams_dict: teams_dict[int(v[1:])])
    return df_f1

@DFunction
def opta_parser_get_season_f1_all():
    """
    returns all f1 feeds for the competitions of this year and the previous
    """
    df_competitions = call('opta_parser_get_competitions_current')
    years_competition_ids = []
    for idx, row in df_competitions.iterrows():
        years_competition_ids.append((row['year'],row['id']))

    dc = DepCollector()
    for i in range(len(years_competition_ids)):
        year = years_competition_ids[i][0]
        competition_id = years_competition_ids[i][1]
        dc.require('opta_get_f1', year, competition_id)
        dc.require('opta_get_team_ids', year, competition_id)
    dc.request()

    df_f1 = []
    df_teams = []
    errored_f1_inds = []
    errored_teams_inds = []
    for i in range(len(years_competition_ids)):
        year = years_competition_ids[i][0]
        competition_id = years_competition_ids[i][1]
        f1 = call('opta_get_f1', year, competition_id)
        teams = call('opta_get_team_ids', year, competition_id)
        # there is teams in f1, but it seems empty and useless:
        # # teams = f1['Team']
        # so we use the ones requested explicitly:
        error = False
        if 'error' in f1:
            if f1['error']['code'] == -32000 and 'Error: competition, feed_type, season_id combination not found in the feed repository' in f1['error']['message']:
                errored_f1_inds.append(i)
                error = True
            else:
                assert False, 'Unknown error: ' + str(f1)
        if type(teams) is dict and 'error' in teams:
            if teams['error']['code'] == 403:
                errored_teams_inds.append(i)
                error = True
            else:
                assert False, 'Unknown error: ' + str(teams)
        if error:
            continue
        # TODO: should use opta_parser_f1_df, instead of duplicating code
        if type(f1) is not dict:
            assert False, f1[:200]
        assert 'data' in f1, str(f1.keys())
        assert type(f1['data']) is dict, type(f1['data'])
        assert 'OptaFeed' in f1['data'], str(f1['data'].keys())
        assert type(f1['data']['OptaFeed']) is dict, type(f1['data']['OptaFeed'])
        assert 'OptaDocument' in f1['data']['OptaFeed'], str(f1['data']['OptaFeed'].keys())

        f1 = f1['data']['OptaFeed']['OptaDocument']
        season_id = f1['@attributes']['season_id']
        assert year == season_id
        assert competition_id == f1['@attributes']['competition_id']

        [t.update({'season_id':season_id}) for t in teams]
        [t.update({'competition_id':competition_id}) for t in teams]
        df_teams += teams

        matchdatas = f1['MatchData']
        for matchdata in matchdatas:
            game_id = matchdata['@attributes']['uID']
            assert game_id[0]=='g'
            date = matchdata['MatchInfo']['dateObj']['utc']
            period = matchdata['MatchInfo']['@attributes']['Period']
            assert period in ['PreMatch', 'FullTime', 'Abandoned', 'Live', 'Postponed'], period
            teams = {
                matchdata['TeamData'][0]['@attributes']['Side']:matchdata['TeamData'][0]['@attributes']['TeamRef'],
                matchdata['TeamData'][1]['@attributes']['Side']:matchdata['TeamData'][1]['@attributes']['TeamRef'],
            }
            assert list(teams.values())[0][0]=='t'
            assert list(teams.values())[1][0]=='t'
            row = {
                'season_id': season_id,
                'competition_id': competition_id,
                'game_id': game_id,
                'datetime': date,
                'period': period,
                'team_home': teams['Home'],
                'team_away': teams['Away'],
            }
            df_f1.append(row)

    df_f1 = pd.DataFrame(df_f1)
    df_teams = pd.DataFrame(df_teams)
    teams_dict = {}
    for ids, row in df_teams.iterrows():                                                                    
        teams_dict[int(row['id'])] = row['name']
    for k in ['home','away']:
        df_f1['team_name_'+k] = df_f1['team_'+k].apply(lambda v, teams_dict=teams_dict: teams_dict[int(v[1:])])
    competitions_dict = {}
    for idx, row in df_competitions.iterrows():
        competitions_dict[int(row['id'])] = row['name']
    df_f1['competition_name'] = df_f1['competition_id'].apply(lambda v, competitions_dict=competitions_dict: competitions_dict[int(v)])
    return {'errors':{'f1_inds':errored_f1_inds,'teams_inds':errored_teams_inds}, 'f1':df_f1, 'teams':df_teams, 'competitions':df_competitions}


@DFunction
def opta_parser_f24_get_half_indicators(game_id):
    """
    counterpart of opta_f24_get_half_indicators
    this one goes to live opt service using opta_parser.jar
    that one uses locally saved xml files
    should return the same thing
    """
    from parsers.opta.f24_parsers import f24_json_game_to_pd, f24_json_events_to_pd, f24_json_qualifiers_to_pd
    from parsers.opta.f24_parsers import f24_minutely_inidcators, f24_minutely_indicators_to_half
    f24_json = call('opta_get_f24', game_id)
    f24_json = f24_json['data']
    df_game = f24_json_game_to_pd(f24_json)
    df_events = f24_json_events_to_pd(f24_json)
    df_qualifiers = f24_json_qualifiers_to_pd(f24_json)
    f24_json_half_indicators = f24_minutely_indicators_to_half(f24_minutely_inidcators(df_game, df_events, df_qualifiers))
    return f24_json_half_indicators

# @DFunction
# def test_opta_half_indicators_xml_eq_json(game_id):
#     """
#     assert opta_parser_f24_get_half_indicators == opta_f24_get_half_indicators
#     """
#     f24_json_half_indicators = call('opta_parser_f24_get_half_indicators', game_id)
#     f24_xml_half_indicators = call('opta_f24_get_half_indicators', game_id)
#     s_xml = f24_xml_half_indicators.groupby(['i_game_uid','s_period','s_indname','b_is_home'])['i_indvalue'].sum()
#     s_json = f24_json_half_indicators.groupby(['i_game_uid','s_period','s_indname','b_is_home'])['i_indvalue'].sum()
#     assert np.linalg.norm((s_xml - s_json).values) == 0, str(s_xml) + " vs " + str(s_json)
#     return True

@DFunction
def opta_parser_live_competitions():
    """
    for each season/competition in opta_parser_get_season_f1_all,
        gets f24 for the last n_last_games and checks if f24 is available.
        calculates the ratio of the games with valid F24 feed for each season_id and competition_id
    returns season_id, competition_id that are thought to be covered by f24
    heavy, run once a week at 6am on Monday
    """
    n_last_games = 3
    op = call('opta_parser_get_season_f1_all')
    f1 = op['f1'].sort_values(['datetime'], ascending=True)
    grp_s_c = f1.groupby(['season_id','competition_id'])

    dc = DepCollector()
    for idx, grp in grp_s_c:
        if len(grp[grp['period']=='PreMatch']) == 0: continue
        grp = grp[grp['period']=='FullTime']
        game_ids = grp.iloc[-n_last_games:]['game_id'].apply(lambda x: int(x[1:])).values.tolist()
        for game_id in game_ids:
            dc.require('call_if_outdated', ['opta_get_f24', [runtime,game_id], None]) # only call if not already called
    dc.request()

    data = []
    for idx, grp in grp_s_c:
        if len(grp[grp['period']=='PreMatch']) == 0: continue
        grp = grp[grp['period']=='FullTime']
        game_ids = grp.iloc[-n_last_games:]['game_id'].apply(lambda x: int(x[1:])).values.tolist()
        n_f24_valid = 0
        for game_id in game_ids:
            assert False, "f24 = call('call_if_outdated', 'opta_get_f24', [runtime,game_id], None)"
            if 'error' in f24:
                assert f24['error']['code'] == -32000
            elif 'data' in f24:
                n_f24_valid += 1
            else:
                assert False, str(f24)[:500]
        row = {'season_id':idx[0], 'competition_id':idx[1], 'f24_ratio':float(n_f24_valid)/n_last_games}
        data.append(row)
    f24_ratio = pd.DataFrame(data)
    live_competitions = f24_ratio[f24_ratio['f24_ratio']>0.5][['competition_id','season_id']]
    live_competitions = pd.merge(live_competitions, op['competitions'], left_on=['competition_id','season_id'], right_on=['id','year'], how='left')
    del live_competitions['id']
    del live_competitions['year']
    return live_competitions

@DFunction
def opta_betfair_competition_matching():
    """
    returns matched opta and betfair competitions
    """
    ratio_limit = 0.7 # discard below this matching ratio we discard
    comps = call('opta_parser_live_competitions')
    bf = call('betfair_soccer_events_all')
    op = call('opta_parser_get_season_f1_all')
    f1 = op['f1']
    f1_live = pd.merge(comps, f1, on=['competition_id','season_id'], how='left')
    bf['ts'] = bf['marketStartTime'].apply(to_int)
    f1_live['ts'] = f1_live['datetime'].apply(to_int)

    # keep games from between 60 days to 1 days ago
    min_days = 1
    max_days = 60
    f1_live = f1_live[f1_live['ts'] < runtime - 1000*60*60*24*min_days]
    f1_live = f1_live[f1_live['ts'] > runtime - 1000*60*60*24*max_days]
    bf = bf[bf['ts'] < runtime - 1000*60*60*24*min_days]
    bf = bf[bf['ts'] > runtime - 1000*60*60*24*max_days]

    from tools.setmatcher import count_common_elements
    bf_grps = bf.groupby(['competition_id', 'competition_name'])
    f1_grps = f1_live.groupby(['season_id','competition_id','competition_name'])
    min_games = 10 # any competition with less than 10 games is noise
    bf_ks = [k for k in bf_grps.groups.keys() if len(bf_grps.get_group(k)) > min_games]
    f1_ks = [k for k in f1_grps.groups.keys() if len(f1_grps.get_group(k)) > min_games and 'Sportec Data Official' not in k[2]]

    common_ratios = {}
    for bf_k in bf_ks:
        for f1_k in f1_ks:
            bf_ts = bf_grps.get_group(bf_k)['ts'].values.tolist()
            f1_ts = f1_grps.get_group(f1_k)['ts'].values.tolist()
            common_ratios[(bf_k,f1_k)] = float(count_common_elements(bf_ts, f1_ts)) / min(len(bf_ts),len(f1_ts))
        
    common_ratios = sorted(list(common_ratios.items()), key=lambda x: -x[1])
    matched_comps = []
    matched_comps_bf = set([])
    matched_comps_f1 = set([])
    for (bf_k, f1_k), ratio in common_ratios:
        if bf_k in matched_comps_bf or f1_k in matched_comps_f1 or ratio < ratio_limit: continue
        matched_comps.append({'bf_competition_id':bf_k[0], 'bf_competition_name':bf_k[1], 'opta_season_id':f1_k[0], 'opta_competition_id':f1_k[1], 'opta_competition_name':f1_k[2], 'matching_score':ratio})
        matched_comps_bf.add(bf_k)
        matched_comps_f1.add(f1_k)
    matched_comps = pd.DataFrame(matched_comps)

    # sending out email with matching - bit unefficient because we need to run the whole of the above twice just for a mail but it's fine for now
    addr_admin = cfg_get('addr_admin')
    subject = "Opta Betfair Comp Matching " + to_str( runtime )[:10]
    body = matched_comps.to_html()
    call('send_email', [addr_admin, subject, body])    
    return matched_comps

@DFunction
def opta_betfair_games_next_hour():
    """
    returns list of matched games that will start within the next hour
    """
    assert False, "op_bf_comps = call('call_if_outdated', 'opta_betfair_competition_matching', [], 'weekly')"
    dc = DepCollector()
    for idx, row in op_bf_comps.iterrows():
        opta_competition_id = row['opta_competition_id']
        opta_season_id = row['opta_season_id']
        dc.require('opta_parser_f1_df', [opta_season_id, opta_competition_id])
    dc.request()

    bf = call('betfair_list_market_catalogue_soccer').dropna()
    bf['ts'] = bf['marketStartTime'].apply(to_int)
    bf['competition_id'] = bf['competition_id'].apply(int)
    ts_min = runtime - runtime%(1000*60*60) + 1000*60*60
    ts_max = runtime - runtime%(1000*60*60) + 1000*60*60*2

    bf_unmatched = pd.DataFrame()
    f1_unmatched = pd.DataFrame()
    bf_f1_matched = pd.DataFrame()
    for idx, row in op_bf_comps.iterrows():
        bf_competition_id = row['bf_competition_id']
        bf_ = bf[bf.apply(lambda x, bf_competition_id=bf_competition_id, ts_min=ts_min, ts_max=ts_max: int(x['competition_id']) == int(bf_competition_id) and x['ts'] >= ts_min and x['ts'] < ts_max, axis=1)]
        opta_competition_id = row['opta_competition_id']
        opta_season_id = row['opta_season_id']
        f1_ = call('opta_parser_f1_df', opta_season_id, opta_competition_id)
        f1_['ts'] = f1_['datetime'].apply(to_int)
        f1_ = f1_[f1_.apply(lambda x, ts_min=ts_min, ts_max=ts_max: x['ts'] >= ts_min and x['ts'] < ts_max, axis=1)]
        if len(bf_) == 0 and len(f1_) == 0:
            continue
        elif len(f1_) == 0:
            bf_unmatched = pd.concat([bf_unmatched, bf_], ignore_index=True)
        elif len(bf_) == 0:
            f1_unmatched = pd.concat([f1_unmatched, f1_], ignore_index=True)
        else:
            def similarity_fun(bf, f1):
                from tools.setmatcher import string_similarity
                bf_event_name = bf[0]
                bf_team_home = bf_event_name.split(' v ')[0]
                bf_team_away = bf_event_name.split(' v ')[1]
                bf_ts = bf[1]
                f1_team_home = f1[0]
                f1_team_away = f1[1]
                f1_ts = f1[2]
                if bf_ts != f1_ts: return float('-inf')
                return string_similarity(bf_team_home, f1_team_home) + string_similarity(bf_team_away, f1_team_away)
            from tools.setmatcher import dataframe_similarity_join, string_similarity
            bf_f1_matched_, bf_unmatched_, f1_unmatched_ = dataframe_similarity_join(bf_, f1_, ['event_name','ts'], ['team_name_home','team_name_away','ts'], similarity_fun = similarity_fun, matching_scores_col = 'score')
            bf_f1_matched = pd.concat([bf_f1_matched, bf_f1_matched_], ignore_index=True)
            bf_unmatched = pd.concat([bf_unmatched, bf_unmatched_], ignore_index=True)
            f1_unmatched = pd.concat([f1_unmatched, f1_unmatched_], ignore_index=True)
    if len(bf_f1_matched) > 0:
        cols = bf_f1_matched.columns.values.tolist()
        cols.remove('competition_name')
        cols.remove('event_name')
        cols.remove('team_name_home')
        cols.remove('team_name_away')
        cols = ['competition_name','event_name','team_name_home','team_name_away'] + cols
        bf_f1_matched = bf_f1_matched[cols]
    return {'bf_f1_matched':bf_f1_matched, 'bf_unmatched':bf_unmatched, 'f1_unmatched':f1_unmatched}

@DFunction
def opta_betfair_games_next_hour_report():
    ret = call('opta_betfair_games_next_hour')
    bf_f1_matched = ret['bf_f1_matched']
    bf_unmatched = ret['bf_unmatched']
    f1_unmatched = ret['f1_unmatched']
    if len(bf_f1_matched) == 0 and len(bf_unmatched) == 0 and len(f1_unmatched) == 0: return
    subject = 'Dagger Opta Betfair Games Next Hour - ' + to_str()[:16]
    body = ""
    if len(bf_f1_matched) > 0:
        body += "<h2>bf_f1_matched</h2>" + bf_f1_matched.to_html()
    if len(bf_unmatched) > 0:
        body += "<h2>bf_unmatched</h2>" + bf_unmatched.to_html()
    if len(f1_unmatched) > 0:
        body += "<h2>f1_unmatched</h2>" + f1_unmatched.to_html()
    addr_admin = cfg_get('addr_admin')
    call('send_email', addr_admin, subject, body)

@DFunction
def opta_betfair_potentially_live_games():
    """
    gets list of potentially live games by going through the past 3 opta_betfair_games_next_hour
    should be called hourly
    """
    hour = 1000*60*60
    games = []
    for i in range(1,4):
        games_ = call('call_if_outdated', ['opta_betfair_games_next_hour', [runtime - i*hour], 'hourly'])['bf_f1_matched']
        for idx, row in games_.iterrows():
            games.append(row.to_dict())
    games = pd.DataFrame(games)
    if len(games) > 0:
        games = games[games['ts'] <= runtime]
    return games

@DFunction
def opta_betfair_potentially_live_games_report():
    """
    should be called hourly
    """
    games = call('opta_betfair_potentially_live_games')
    if len(games) == 0: return
    subject = 'Dagger Opta Betfair Games Potentially Live - ' + to_str()[:16]
    body = "<h2>Games</h2>" + games.to_html()
    addr_admin = cfg_get('addr_admin')
    call('send_email', addr_admin, subject, body)

@DFunction
def opta_betfair_check_half_time_games():
    """
    called minutely
    """
    half_time_minutes_after_game_start = 55 # we check 55 mins after game start
    assert False, "games = call('call_if_outdated', 'opta_betfair_potentially_live_games', [], 'hourly')"
    df = []
    dc = DepCollector()
    for idx, row in games.iterrows():
        if row['ts']/1000/60 == runtime/1000/60 - half_time_minutes_after_game_start: # doing /1000/60 to avoind sub-minute timestamp mismatch errors
            opta_game_id = row['game_id']
            assert opta_game_id[0] == 'g'
            opta_game_id = int(opta_game_id[1:])
            bf_event_id = int(row['event_id'])
            dc.require('opta_betfair_half_time_game', [opta_game_id, bf_event_id])
            df.append(row.to_dict())
    dc.request()
    df = pd.DataFrame(df)
    if len(df) == 0: return
    subject = 'Dagger Opta Betfair Games Half Time - ' + to_str()[:16]
    body = "<h2>Games</h2>" + df.to_html()
    addr_admin = cfg_get('addr_admin')
    call('send_email', addr_admin, subject, body)

    dc = DepCollector()
    for idx, row in games.iterrows():
        if row['ts']/1000/60 == runtime/1000/60 - half_time_minutes_after_game_start: # doing /1000/60 to avoind sub-minute timestamp mismatch errors
            opta_game_id = row['game_id']
            assert opta_game_id[0] == 'g'
            opta_game_id = int(opta_game_id[1:])
            bf_event_id = int(row['event_id'])
            opta_betfair_half_time_game = call('opta_betfair_half_time_game', opta_game_id, bf_event_id)
            subject = 'Dagger Opta Betfair Games Half Time - ' + str(opta_game_id) + ' - ' + str(bf_event_id) + ' - ' + to_str()[:16]
            body = ""
            if opta_betfair_half_time_game['status'] == 'OK':
                body += "<h2>opta_events_df</h2>" + opta_betfair_half_time_game['opta_events_df'].to_html()
                body += "<h2>bf_catalogue</h2>" + json.dumps(opta_betfair_half_time_game['bf_catalogue'], indent=4)
                body += "<h2>bf_market_book</h2>" + json.dumps(opta_betfair_half_time_game['bf_market_book'], indent=4)
            elif opta_betfair_half_time_game['status'] == 'Error':
                body += opta_betfair_half_time_game['message']
            else:
                assert False, str(opta_betfair_half_time_game['status'])
            dc.require('send_email', [addr_admin, subject, body])
    dc.request()

@DFunction
def opta_betfair_half_time_game(opta_game_id, bf_event_id):
    assert type(opta_game_id) is int
    assert type(bf_event_id) is int
    opta_f24 = call('opta_get_f24', opta_game_id)
    if 'error' in opta_f24:
        assert opta_f24['error']['code'] == -32000 and 'not found in the feed repository' in opta_f24['error']['message'], str(opta_f24)
        return {
            'status': 'Error',
            'message': opta_f24['error']['message'],
            'code': opta_f24['error']['code'],
        }
    bf_catalogue = call('betfair_list_market_catalogue_by_event_type_id_or_event_id', None, bf_event_id)
    marketIds = [ c["marketId"] for c in bf_catalogue ]
    bf_market_book = call('betfair_list_market_book', marketIds)
    opta_f24 = opta_f24['data']
    from parsers.opta.f24_parsers import f24_json_game_to_pd, f24_json_events_to_pd, f24_json_qualifiers_to_pd
    opta_events_df = f24_json_events_to_pd(opta_f24)
    return {
        'status': 'OK',
        'bf_catalogue': bf_catalogue,
        'bf_market_book': bf_market_book,
        'opta_events_df': opta_events_df,
    }

# @DFunction
# def opta_parser_f24_all_instances(game_id):
#     """
#     returns a dataframe of all previous instances of calling an f24 feed with cols:
#         runtime, period_id, min, sec
#     used for time synchronisation for example
#     """
#     prev_runtime = prevRuntime('opta_parser_f24_all_instances', [], check_in_postgres = False)
#     f24_all_instances = []
#     if prev_runtime is not None:
#         f24_all_instances = call('opta_parser_f24_all_instances', ts=prev_runtime)
#         f24_all_instances = f24_all_instances.T.to_dict().values()
#     f24_all_runtimes = set([])
#     for row in f24_all_instances:
#         f24_all_runtimes.add(row['runtime'])

#     from dagger import Globals
#     cache = Globals.get_cache()
#     runtimes_and_f24 = cache.get_all_runtimes_and_values(DCall('opta_get_f24',0, game_id))
#     for row in runtimes_and_f24:
#         rt = row[0]
#         if rt in f24_all_runtimes: continue
#         f24_vref = row[1]
#         f24 = cache.value.resolveVRef(f24_vref)
#         from parsers.opta.f24_parsers import f24_json_events_to_pd
#         f24_events = f24_json_events_to_pd(f24)
#         if len(f24_events) > 0:
#             period_id = f24_events.iloc[-1]['period_id']
#             min = f24_events.iloc[-1]['min']
#             sec = f24_events.iloc[-1]['sec']
#         else:
#             period_id = None
#             min = None
#             sec = None
#         f24_all_instances.append({'runtime':rt, 'period_id':period_id, 'min':min, 'sec':sec})
#     return pd.DataFrame(f24_all_instances)
