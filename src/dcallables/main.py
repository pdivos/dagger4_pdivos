from dagger4 import DFunction, call, Fiddle

# register user serilaizers
from register_user_serializers import register
register()

# import triggers executions of @DLambda and @DFunction decorators
from . import base, betfair, opta

@DFunction
def main():
    # return call('test_betfair')
    # return call('test_tor')
    # return call('opta_get_f24', 641698)
    # return call('opta_get_f1', 2017, 8)
    # return call('opta_get_competition_ids', 2017)
    # return call('opta_get_team_ids', 2017, 8)
    # return call('opta_get_player_ids', 2017, 8, 3)
    return call('opta_parser_get_season_f1_all')

