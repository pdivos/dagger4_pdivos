# -*- coding: utf-8 -*-
from dagger4 import DFunction, DLambda, call, Fiddle, ts

@DFunction
def urlget(url):
    import urllib.request, urllib.error, urllib.parse
    #from utils import timeutils
    #if - ts + timenow() > 1000 * 60 * 60:
    #    raise Exception('we are out by more than an hour, this should not run')
    assert type(url) is str
    res = urllib.request.urlopen(url)
    if res.info().get('Content-Encoding') == 'gzip':
        from io import StringIO
        import gzip
        buf = StringIO(res.read())
        f = gzip.GzipFile(fileobj=buf)
        return f.read()
    return res.read()

@DLambda
def config_passwd():
    assert False, "config not fiddled"

@DLambda
def config(key):
    from utils.config import cfg_get
    return cfg_get(key, call('config_passwd'))

@DFunction
def send_email(address, subject, body):
    gmail_user = call('config','google_user')
    gmail_pwd = call('config','google_password')
    from utils.email_ import send_email as _send_email
    _send_email(address, subject, body, gmail_user, gmail_pwd)

@DFunction
def send_email_with_attachments(address, subject, body, attachments):
    """
    attachment is a dict of fname -> string which will be attached to the mail
    call('send_email_with_attachment', ["pdivos@gmail.com", "tmp", "", {"tmp.csv":df.to_csv(encoding='utf-8')}])
    """
    gmail_user = call('config','google_user')
    gmail_pwd = call('config','google_password')
    from utils.email_ import send_email as _send_email
    _send_email(address, subject, body, gmail_user, gmail_pwd, attachments = attachments)

# @DFunction
# def http_get(url):
#     """
#     same as urlget but this is used for downloading and extracting .gz files
#     """
#     import requests
#     r = requests.get(url, allow_redirects=True)
#     assert r.status_code == 200
#     assert type(r.content) is bytes
#     if r.headers['Content-Type'] == 'application/gzip':
#         import gzip, io
#         filename = None
#         mode = 'rb'
#         fileobj = io.BytesIO(r.content)
#         compresslevel = None
#         gz = gzip.GzipFile(filename, mode, compresslevel, fileobj)
#         s = gz.read()
#         gz.close()
#         return str(s,'utf-8')
#     else:
#         assert False, "unknown content type: " + r.headers['Content-Type']

# @DFunction
# def urlpost(url, data):
#     import requests
#     resp = requests.post(url, data=data)
#     assert resp.status_code == 200
#     return resp.text

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# @DFunction
# def send_email(address, subject, body):
#     from utils.email_ import send_email as _send_email
#     _send_email(address, subject, body)

# @DFunction
# def send_email_with_attachment(address, subject, body, attachments):
#     """
#     attachment is a dict of fname -> string which will be attached to the mail
#     DUtils.reinsertExec('send_email_with_attachment', [ts, "pdivos@gmail.com", "tmp", "", {"tmp.csv":df.to_csv(encoding='utf-8')}])
#     """
#     from utils.email_ import send_email as _send_email
#     _send_email(address, subject, body, attachments = attachments)

# @DFunction
# def gspread_write_sheet(workbook_title, worksheet_name, data, by_key):
#     from tools.gspread import write_sheet
#     if type(data) == pd.DataFrame:
#         df = data.fillna("")
#         value = df.values.tolist()
#         columns = df.columns.values.tolist()
#         index = df.index.values.tolist()
#         data = [[""] + columns]
#         for ii in range(len(index)):
#             data.append([index[ii]] + value[ii])
#         data[0][0] = to_str(now())
#     write_sheet(workbook_title, worksheet_name, data, by_key)

# @DFunction
# def gspread_read_sheet(workbook_title, worksheet_name, by_key):
#     assert by_key == True or by_key == False
#     from tools.gspread import read_sheet
#     return read_sheet(workbook_title, worksheet_name, by_key = by_key)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# @DFunction
# def smarkets_oddsfeed():
#     url = 'http://odds.smarkets.com/oddsfeed.xml.gz'
#     return call('http_get', [url])

# @DFunction
# def smarkets_events():
#     from lxml import etree
#     xml = call('smarkets_oddsfeed', [])
#     _head = "<?xml version='1.0' encoding='utf-8'?>"
#     assert xml.startswith(_head)
#     xml = xml[len(_head):]
#     tree = etree.fromstring(xml) 
#     data = []
#     cols = ['id','type','name','slug','parent','parent_slug','date','time','url']
#     #<event date="2017-09-22" id="855098" name="Luckenwalde vs. Wacker Nordhausen" parent="Germany Regionalliga 2017-2018" parent_slug="germany-regionalliga-2017-2018" slug="fsv-63-luckenwalde-fsv-wacker-90-nordhausen-2017-09-22" time="17:30:00" type="Football match" url="/sport/football/germany-regionalliga/2017/09/22/fsv-63-luckenwalde-vs-fsv-wacker-90-nordhausen">
#     for el in tree.xpath('.//event'):
#         data.append([el.attrib.get(c) for c in cols])
#     return pd.DataFrame(data=data, columns=cols)

# @DFunction
# def smarkets_prices_politics():
#     filters = {'event_type':'Politics'}
#     return call('smarkets_prices_df', [filters])

# @DFunction
# def smarkets_prices_df(filters):
#     filters = filters or {}
#     from lxml import etree
#     xml = call('smarkets_oddsfeed', [])
#     _head = "<?xml version='1.0' encoding='utf-8'?>"
#     assert xml.startswith(_head)
#     xml = xml[len(_head):]
#     tree = etree.fromstring(xml)    
#     assert tree.tag == 'odds'
#     def prices_from_contract(contract):
#         assert contract.tag == 'contract'
#         prices = {}
#         for bidoffers in contract:
#             # <bids/> <offers/>
#             bidoffer = 'bid' if bidoffers.tag == 'bids' else 'offer'
#             assert bidoffers.tag in ['bids','offers']
#             for price in bidoffers:
#                 # <price backers_stake="485.27" decimal="1.50" liability="970.68" percent="66.67"/>
#                 price_backers_stake=float(price.attrib['backers_stake'])
#                 price_liability=float(price.attrib['liability'])
#                 price_percent=float(price.attrib['percent']) / 100.0
#                 price_decimal=float(price.attrib['decimal'])
#                 assert abs(1.0/price_decimal - price_percent) < 1e-3, etree.tostring(price)
#                 price_notional = price_liability + price_backers_stake
#                 price_percent_recalc = (price_backers_stake if bidoffer == 'bid' else price_liability)
#                 if price_notional > 10:
#                     assert abs(price_percent - price_percent_recalc / price_notional) < 1e-3, str(etree.tostring(price)) + bidoffer + str(price_percent) + "-" + str(price_percent_recalc) + "/" + str(price_notional)
#                 prices[bidoffer] = (price_percent, price_notional)
#                 break
#             for k in ['bid','offer']:
#                 if k not in prices:
#                     prices[k] = (None,None)
#         return prices
#     event_cols = ['id','type','name','slug','parent','parent_slug','date','time','url']
#     market_cols = ['winners', 'slug', 'traded_volume', 'id']
#     contract_cols = ['id','name','slug']
#     data = []
#     for event in tree:
#         #<event date="2017-09-22" id="855098" name="Luckenwalde vs. Wacker Nordhausen" parent="Germany Regionalliga 2017-2018" parent_slug="germany-regionalliga-2017-2018" slug="fsv-63-luckenwalde-fsv-wacker-90-nordhausen-2017-09-22" time="17:30:00" type="Football match" url="/sport/football/germany-regionalliga/2017/09/22/fsv-63-luckenwalde-vs-fsv-wacker-90-nordhausen">\
#         assert event.tag == 'event'
#         if filters.get('event_type') is not None and event.attrib['type'] != filters.get('event_type'):
#             continue
#         for market in event:
#             assert market.tag == 'market'
#             # {'winners': '1', 'slug': 'vat-scope-widened', 'traded_volume': '0', 'id': '103064'}
#             for contract in market:
#                 assert contract.tag == 'contract'
#                 prices = prices_from_contract(contract)
#                 row = {'bid_price':prices['bid'][0],'offer_price':prices['offer'][0],'bid_ntl':prices['bid'][1],'offer_ntl':prices['offer'][1]}
#                 row.update({'contract_'+c:contract.attrib.get(c) for c in contract_cols})
#                 row.update({'market_'+c:market.attrib.get(c) for c in market_cols})
#                 row.update({'event_'+c:event.attrib.get(c) for c in event_cols})
#                 data.append(row)
#     assert len(data) > 0
#     df = pd.DataFrame(data)
#     cols = []
#     cols += ['contract_'+c for c in contract_cols]
#     cols += ['market_'+c for c in market_cols]
#     cols += ['event_'+c for c in event_cols]
#     cols += ['bid_price','offer_price','bid_ntl','offer_ntl']
#     assert set(df.columns.values.tolist()) == set(cols)
#     df = df[cols]
#     return df

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# minutely is inserted by app_crontab which should be running in a separate process


# @DFunction
# def minutely():
#     daily_hour = 6 # 6am
#     weekly_day = 4 # Monday
#     # we use DepCollector because we want all these calls to be inserted to queue at once so they get processed in parallel
#     # we don't call any of the functions because we don't need their return value
#     dc = DepCollector()
#     if ts % (1000*60*60) == 0: dc.require('hourly',[])
#     if ts % (1000*60*60*24) == 1000*60*60*daily_hour: dc.require('daily',[]) # 6 for 6am
#     if ts % (1000*60*60*24*7) == 1000*60*60*(daily_hour+weekly_day*24): dc.require('weekly',[]) # 6+4*24 for 6am on Monday morning
#     if ts % (1000*60*60) == 0: dc.require('redis_save',[])
#     dc.request()

# @DFunction
# def hourly():
#     # we use DepCollector because we want all these calls to be inserted to queue at once so they get processed in parallel
#     # we don't call any of the functions because we don't need their return value
#     dc = DepCollector()
#     dc.require('politicalbetting_diff', [])
#     dc.require('news_api_diff_send_report', [])
#     dc.require('crypto_main',[])
#     dc.request()

# @DFunction
# def daily():
#     # we use DepCollector because we want all these calls to be inserted to queue at once so they get processed in parallel
#     # we don't call any of the functions because we don't need their return value
#     dc = DepCollector()
#     dc.require('betfair_main',[])
#     dc.require('cryptoccy_pnl_send_report',[])
#     dc.require('runningprojects_send_report',[])
#     dc.require('reminders_send_report',[])
#     dc.require('cache_info_send_report',[])
#     dc.require('send_email_dfun', ['pdivos@gmail.com', 'Football - Betfair vs. Preds - ' + to_str(ts).split(' ')[0], 'football_predictions_betfair_get_portfolio', [ts]])
#     dc.require('reminders_send_individual_mails', ['daily'])
#     dc.request()

# @DFunction
# def weekly():
#     # we use DepCollector because we want all these calls to be inserted to queue at once so they get processed in parallel
#     # we don't call any of the functions because we don't need their return value
#     dc = DepCollector()
#     dc.require('reminders_send_individual_mails', ['weekly'])
#     dc.request()
