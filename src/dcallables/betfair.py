from dagger4 import DFunction, DLambda, call, Fiddle

import betfairlightweight

@DFunction
def betfair_creds():
    betfair_user = call('config','betfair_user')
    betfair_password = call('config','betfair_password')
    betfair_app_key_live = call('config','betfair_app_key_live')
    return {'user': betfair_user, 'pass': betfair_password, 'api_key': betfair_app_key_live}

@DFunction
def betfair_login():
    betfair_crt_file = call('config','betfair_crt_file')
    betfair_key_file = call('config','betfair_key_file')

    betfair_creds = call('betfair_creds')
    betfair_user = betfair_creds['user']
    betfair_password = betfair_creds['pass']
    betfair_app_key_live = betfair_creds['api_key']

    certs = '/certs/'
    import os
    path = os.path.join(os.pardir, certs)
    fname_crt = path + '/client-2048.crt'
    fname_key = path + '/client-2048.key'
    os.system('mkdir -p '+path)
    with open(fname_crt, 'w') as f:
        f.write(betfair_crt_file)
    with open(fname_key, 'w') as f:
        f.write(betfair_key_file)
    
    bf = betfairlightweight.APIClient(betfair_user, betfair_password, app_key=betfair_app_key_live)
    bf.login()
    return bf.session_token

@DFunction
def test_betfair():
    session_token = call('betfair_login')
    betfair_creds = call('betfair_creds')
    betfair_user = betfair_creds['user']
    betfair_password = betfair_creds['pass']
    betfair_app_key_live = betfair_creds['api_key']
    bf = betfairlightweight.APIClient(betfair_user, betfair_password, app_key=betfair_app_key_live)
    bf.session_token = session_token
    
    return bf.account.get_account_funds().json()