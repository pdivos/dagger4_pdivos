"""
OS related tools
"""

import inspect, os

def run_command(cmd):
    from subprocess import Popen, PIPE
    p = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    return { 'stdout': stdout, 'stderr': stderr, 'returncode': p.returncode }

def get_env(k, default = None):
    from os import environ
    if default is None:
        assert k in environ, 'Please specify env ' + k
    else:
        if k not in environ:
            return default
    return environ[k]

def _VmB(VmKey):
    _proc_status = '/proc/%d/status' % pid()
    _scale = {'kB': 1024.0, 'mB': 1024.0*1024.0,
              'KB': 1024.0, 'MB': 1024.0*1024.0}
     # get pseudo file  /proc/<pid>/status
    try:
        t = open(_proc_status)
        v = t.read()
        t.close()
    except:
        return 0.0  # non-Linux?
     # get VmKey line e.g. 'VmRSS:  9999  kB\n ...'
    i = v.index(VmKey)
    v = v[i:].split(None, 3)  # whitespace
    if len(v) < 3:
        return 0.0  # invalid format?
     # convert Vm value to bytes
    return float(v[1]) * _scale[v[2]]

def memory(key):
    import os, psutil
    memory_info = psutil.Process(os.getpid()).memory_info()
    if key == 'Total':
        return memory_info.vms
        # return _VmB('VmSize:') # old mode
    elif key == 'RAM':
        return memory_info.rss
        # return _VmB('VmRSS:') # old mode
    else:
        assert False, key

def memory_Total():
    '''
    Return memory usage in bytes.
    '''
    return memory('Total')


def memory_RAM():
    '''
    Return resident memory usage in bytes.
    '''
    return memory('RAM')

_pid = None
def pid():
    global _pid
    if _pid is None:
        import os
        _pid = os.getpid()
    return _pid

_host = None
def host():
    global _host
    if _host is None:
        import socket
        _host = socket.gethostname()
    return _host

_memory_limit = None
def get_memory_limit():
    global _memory_limit
    if _memory_limit is None:
        from tools.system import get_env
        memory_limit = get_env('MEMORY_LIMIT')
        assert memory_limit[-1] in ['b','k','m','g'], "invalid format for MEMORY_LIMIT, must end with one of [b,k,m,g], instead have: " + memory_limit
        memory_limit = int(memory_limit[:-1]) * (2**({'b':0,'k':10,'m':20,'g':30}[memory_limit[-1]]))
        _memory_limit = memory_limit
    return _memory_limit

def get_memory_limit_ratio():
    memory_limit = get_memory_limit()
    memory = memory_RAM()
    return float(memory) / memory_limit
    
def assert_memory_limit():
    """
    enforces a soft memory limit passed in through env MEMORY_LIMIT by throwing if exceeded
    """
    memory_limit = get_memory_limit()
    memory = memory_RAM()
    assert memory < memory_limit, "Memory limit of " + "%.2f"%(float(memory_limit)/1024**2) + "MB exceeded by using " + "%.2f"%(float(memory)/1024**2) + "MB"
