"""
it's a shame but we need to suffix file name with an underscore otherwise
no script starting from the same folder can import pandas because pandas will clash with email.py
"""

def send_email(recipient, subject, body, gmail_user, gmail_pwd, attachments = None):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = gmail_user
    msg['To'] = ','.join(recipient) if type(recipient) is list else recipient

    # if add_footer:
    #     body += footer_sent_by()

    text = body
    html = body

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # attach the file contents
    if attachments is not None:
        from email.mime.application import MIMEApplication
        assert type(attachments) is dict, "attachments must be a dict of fname -> content string"
        for fname, contents in attachments.items():
            if type(contents) is str:
                contents = contents.encode('utf-8')
            part = MIMEApplication(contents, Name=fname)
            part['Content-Disposition'] = 'attachment; filename="%s"' % fname
            msg.attach(part)

    TO = recipient if type(recipient) is list else [recipient]

    # Prepare actual message
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login(gmail_user, gmail_pwd)
    server.sendmail(gmail_user, TO, msg.as_string())
    server.close()
    print('mail', subject, 'sent to', TO)

# def footer_sent_by():
#     from dagger.DFunction import Globals
#     from dagger.DCore import DNodeKeyRunning
#     from tools.config import cfg_get
#     dcacheviewer_host = cfg_get('dcacheviewer_host')
#     import socket
#     worker_host = socket.gethostbyname(socket.gethostname())
#     if Globals.get_dnodekey() is None:
#         return "<h5>sent from " + worker_host + "</h5>"
#     dnodekeyrunning = Globals.get_dnodekey()
#     import urllib.request, urllib.parse, urllib.error
#     link = "<a href='" + dcacheviewer_host + "/getDCall?dnodekeyrunning=" + dnodekeyrunning.to_url() + "'>" + str(dnodekeyrunning) + "</a>"
#     return "<br><div style='font-size:75%'>sent by " + link + " from " + worker_host + "</div>"
