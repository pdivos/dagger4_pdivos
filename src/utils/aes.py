import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES as aes

def aes_encrypt(value, passwd):
    import base64
    import hashlib
    from Crypto import Random
    from Crypto.Cipher import AES as aes
    passwd = hashlib.sha256(bytes(passwd, 'utf-8')).digest()
    def _pad(s, bs):
        return s + (bs - len(s) % bs) * chr(bs - len(s) % bs)
    value = _pad(value, 32)
    iv = Random.new().read(aes.block_size)
    cipher = aes.new(passwd, aes.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(value))

def aes_decrypt(value, passwd):
    import base64
    import hashlib
    from Crypto import Random
    from Crypto.Cipher import AES as aes
    passwd = hashlib.sha256(bytes(passwd, 'utf-8')).digest()
    value = base64.b64decode(value)
    iv = value[:aes.block_size]
    cipher = aes.new(passwd, aes.MODE_CBC, iv)
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]
    return str(_unpad(cipher.decrypt(value[aes.block_size:])), 'utf-8')

def aes_decrypt_using_env(encoded_value, env):
    from utils.system import get_env
    key = get_env(env)
    return aes_decrypt(encoded_value, key)
