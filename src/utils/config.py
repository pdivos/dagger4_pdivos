"""
this file stores all the config values such as passwords
we have two sections: one for unencrypted stuff and one for encrypted stuff
the main method is get_cfg that returns both encrypted and unencrypted
in order for the decryption to work, the correct password must be passed in through the env CFG_PASSWORD
if you only access unencrypted stuff there is no need to pass in a password to env CFG_PASSWORD
if you want to add a new encrypted entry:
    1. add it unencrypted to values
    2. run this file so that __main__runs.
       this will do the encryption and print the encrypted to stdout
       also will decrypt all encrypted ones and print the to sdtout as well
    3. copy from stdout and paste into encoded_values
    4. remove from the unencrypted one from values
"""

import json

values = {
    'addr_clients': ["pdivos@gmail.com"],
    'addr_admin': "pdivos@gmail.com",
    'my_phonenumber': '+447450742130',
    'dagger4_auth_token': '602a5b703c00bf7dedec61152befd9f3e15e05b3e6d34d73ee205727f702ac18',
    'opta_f24_folder': '/data/opta/F24/',
    'output_folder': '/output/',
    'reuters_news_api_key': '93bc3976801f4296bdeb9b56df67801c'
}

encoded_values = {
    'google_user': 'JpCOXFUNWb1eO0Wi3jHeq9KI/HXIkIUgQHvRnNOdhU66zNk+XjX79k4S1SoxP+cx',
    'google_password': 'KVuwh6LgRlec1HvGsYCtw+NaaGpimj+Zmo7/0YVK1RstRYb36g7WmeqV1Qz74wua',
    'twilio_phonenumber': 'XuJRlBZ8heWqbktIUxQMy/Ys1CqAsfSiPZdqKs1TdV+fZM1WvXFNWTfG+o795kCO',
    'twilio_account_sid': 'gdZIMxczzkD2DP0TinA5Azmmu8iIlGU+dTFw32bN4NJeOvdHLlDYRwD24B73lSFOQE+9dN3ebZyU98WTYBA0QA35jmyESSUM9jaAqlyn10c=',
    'twilio_auth_token': 'N5MrEtsohJKeGBldw7fcWwFFqBXcK7uWAbkZk1j9JzGw1eNqSv8gpSlyxniIz+A0hjOqQTU7ycV1S6Tc57WfR4aMPoSl5Sxg1cdAwQzHsmw=',
    'dropbox_access_token': '5hj33ziwRtHIU+3nT7/uRni1nev9IBrxnAep3atIXuTX3Qbd0x5xqwh7BlRBvRe9ry3yIdgnkWnXC/uYk/olg62I8Su86aMdwMeu38SzlTTCFUfIMqurUc5Z//ZdRofDieXAWp6F6DbII+f6OfAiZg==',
    'dropbox_app_secret': 'WUAi3mtjiK1plJAzWzAwT9DchXvDP3oPEuRPJbsG4Eh/litoXObh84UkIVBCXjrz',
    'dropbox_app_key': 'Nhofj3xiRuTBE97iNPIWZGUv5CgJqKoKn04L/TNrr2AGsFA3s2yYnHcS5poLc6rf',
    'deribit_secret': 'm7nPK/YfQr2nVWzsqDxi3J1vfe2g9L0it+ZW0w0QjGxcC/j1WfZvngBzy6qSW5JZ/5+umFm32Ma/D2PYnOYxkWI/565ndEtkBJxZd47HbXs=',
    'deribit_token': 'Wz3RSn5N0JxyM7EaNU9Q/xbXwtMMW4yAlx7bEpRj6Tet7GIU4nUWFQmhwrgMJcL9',
    'betfair_user': 'Md8IA5SmcuB5+Izi2hEkFcwtDUFDbz/3Uj//Wbh8VnwbdTZ7xnWoZifE4xrp11L/',
    'betfair_password': 'gKA3BOr+4x+KPkjcBgW6TEO3dcW9jh9vaEWsbYlZs4GYgFATm2JcUraHshOoeenE',
    # follow this to generate key and cert: https://docs.developer.betfair.com/display/1smk3cen4v3lu3yomq5qye0ni/Non-Interactive+%28bot%29+login
    # this creates 3 files: pem, crt, key
    # upload pem in your betfair account as explained in the guide
    # the crt and key files below will need to be saved in the /cert/ folder so that betfairlightweight can pick them up:
    'betfair_crt_file': 'T0zLmRJEiBBsmI2d42D3CNyY4dsbfeKw9wD+thWTgRRq9OZctVdxmAaq7WDxN64FFTtmYvSsIHP1xbpx2OZLWhLEUToxt007R4ODE2R0HxFdDz90u8h3yJBA4BAt3Ih6oMOh8JU3KWO2hso/fRwM/ilNc4iv5kUOyqU26I59fH2FClDOoTmvNgYQvMDtZnderwPHoCQmGNaV2T8ObNIZTv4wGy2MgE+3MO1eHsSFyh0Zjn7VnIABLainDNffKnDFWGI7rcVfhpDB5khGIq7QEH+Xf+oeVmHOWQ+ZV+W9xgtNP6Oq4w3GeNMQajICXWpPWjq740qvyh4+1vhnEJwsq8rRduQaClEy50ahfJCikjYdZnQpb9vyUkQ0qysy6HyzTL9rqyLtl0DDD4jPLjs88ILoBCZVhctrebJIWmDhAzx19VzRdIeDYOsI0llKWBa5oxCbasq6nBi8RXjQmCI0DNDyentg/ZwRYiLtSH+QfSDdyzJt5G5F4SNIpvrx5unvgV7RkHo8+1R/lrq/LWJfDsllOQW20eBD6Ne+cjgziv7VzXwfW6DQZfPfl8AVjK6f7aWmIvXNqMYyAEdnt5d71T0q3s5FOQFNRObakaKncfpP5WK4fRmN9LRzCh7Mk1NrNkhfgyDSEd3PNB6Pro0JQQOrJJcSg1BkABdEebMxX6zA42botf/065rlQKD5jlhdXcIX2Mt6YzhfD4/XG1DOCyCk91FfxegctbmMdPLDOaYlm5E9XnzswGMONzkGnfAgPD4unQL70ERCfKX+odsZr8E4zhDs16LYwaYEt9mAuLLGvy2FbZr3Ove7H/IXhoRbeFt18FLa0wjfLlsQ/vcW3UjwZUVLYdfT+YJ6fsp2q3A6u249o+2Snrm6ndKv9ZGLMP2gp1QFIBYBbFO8cyQIQ7ttE4fLueUFitrsORMxcDE9ykd1MudQa7PI8ru+imklTwa5BagrYvqjfQA92txwTBGLXwZvLrja7aJ44VGKK1xwsOW9p6SU+hZUyuKMYVCVPWeLv2qa+mV9LN8RtJuZQ4xpjbpwS1U0UWI0cCU21a2ezyNpETGJ9q2fPKMAGB9HCe4uKp0vweIIxhKNh/w03JsIOsnmKgaEj3dTzVxYGZZuD4XLpJtUz+Zjo9mi+VEQOSvZZvkduzuG/mBzdJeirifUymow5UP/o4ZfVtc2789oan1fRY2WLniYR77mcFOAeUvCGJaCfn0o7JFGLQA/wsRSYDQDlLNXrMCZL3YJHbMEOR5VcZZRb3L0UDyMO6nfrcxdGa6LlDQl4ofOmD8zgMgq4GEmsryIM2LUUl+E85fwm83iRZTyE8dhnKr3n296IKOxO6hs8gguY05SV+Y1Hj5ycUQwv40y4QZL0NiuEi+CpfaDJMACFtm7d9vUDzMKs+pxOsJU0vPrkaHI4j4srlxP48inQp96JRPdM2QgJCbAWLIQehgl/Kt4k/2c1BJcTAZA8Ir/XotgID3KJV02KeUH78F/Vr0E8bmKnfngKOQWYGF4GOXYuk63CC+PMHzaHQ9sFTYmCSwx8V3Q/9piGkM3pHQltmsLC0kAuPbX+6qHYaMA3nhTSrvvTxC0HTEDbzWoukQDeWYBduKvQgJ29pRU/uowwdDz4aR3HDyqrCw9hrHSldZxiMF64xngOLNjj5HrDfV25OJwbQmp4EkXI+RvTaev3qsrsRZRoo2y2vH2H7ngYBuT4Gg7RBnKuFzQDrkNkNWtT0lz7cL8FxOEXnU9gG22Q9Xl/jVG3OTvB/eQhjPZDnLBizRB4jhjkphhWH5FnpGPfWlndbGJ1IkcAzn1cLk944c+wL5c0ht3YpxwIJrkiN6yrjq/eYiO+Kal/4qhDM+03m6doYnBRLK7fYgWrsYR7wPcEU4DQm6A2c0=',
    'betfair_key_file': 'feIZN2EDx+2skMYAjJbyGEwaYHb3CuN3Yjqpy5ufzETBJLsqWSoODhFGINyf/yelMbFwujQcnD8GETqE/Jfn9u11Wl52XUgg1Mf/inV2OOKzOS3uabIj9XcaIvcK+L04qGSDLrYe8pezdozKEPu8aaXMjv+hf+4sijlI7X5ypRhkNjC6ORXVKwunayFGOw2cD3r0n9YB6Ec/N1ApX7LQa7BVXx3KwnrUJYNWHznIpEpVXpgpWaDDw0oJvPfvVUiYc54HYXWUBQ7wXXPcrZsmbirzrVoeqk05Oe9LnxlWNL8SRLiR+FgSt0Kikuk9bK3L/ezOqL1YL98p6SF5uqLAxTkgGxIhGuLZrVgEtTBHZxRatYoRtzH1mOqDAduFoq78zjCSITsJzB635aTC7NtKoQjfZn71j04Qg36dz6EmNRdqcrbrds0HAZJVd/IvruRvxSkrSUwqBFpbcttreJ/2OIxeOAoXIgeW/f5v+Bj7v02Ogy53p4+rlogUbazh4F+aXpk0ke7X6h2joF5g4ggZ6LDHS5QSpPF/bjuiG7RyhT1i69eUCWD/xLaWocUmxWc8XJwc0lOm1/K+Jg0d9b1UPYDk8qQ/nbPUCn90iMdRSFjkeq057PK7yitj5TJZtZoOt9eUxkR30rn+gXtucgRWQ7e8ULCvPWzJLypBB7tIh8kYmHWqNV90eVIpC/sPZTvO1UEo5gqmc4wOViNSb/fkyG+2Gj2QVvXqnkAVDhXJhrtFU0gan0Jdov89ZIUZ3Zb3xLATjkbC1zIg0gnlSY4mJRZrwUcoWiHyfPXl2AY3DUU35u2ogR3CYgkgPsDHdWxeWDeiQNx7kxZq32p8sbB+74YMMW2dQRNnfEcl8RH84LfaMKgeMVwk+vKZpz13LnypgynaTVy0LzhZeBNiRB+4Dp8P0BaXD/ZYfieG0qrvOLKpL9Qj7znR5SA0Z9zJVIdhrZrcWpOkAdlbKkX8pJx5sf1cxd86kpOYlbmpLTqCWFY2AjuWo88EMjPA7pSjl5FhAqRrUrATJDEVf9Z16xBAWoNAuBj4gX2yJQi3v5BzaMTaUmZzHPzDKnUGy2AwQJP3Y0/pe+RDPfcljKEgt1GNdqqzn//ARDUOkyNrEmGnUSol+Q+LYXTQgY569NkEG9ZX1bDL+RMSg0ooKgR1hEVLp9KJhmeTUIuhQXDqrgH+6AI0NvObT+9JMZt5yRB8WvBo8yhfNHoifGfX3kogEbKKF9FxlkZwVcU1jlumaZQTBbkmjxfXgp/weQ63djzQR7sxs5/XlyMXIBdw/vPa8YmqBwWZuZ08K2whOS47iQ4/bgW6fiCP+pSgVfHTLwa6ua0WzKRLHAagYCSe188V8G/Y1IyS0y4FJm/DN5XeeKvMlMlObn7oiPMTwtqXKfW84GiK3BkaYQCZ+/NF9gmknGj4WeV4f1gkr+GcmnU1lrzPt3qlzbSwkh4+7XaJkjvhWOZgcSUd6588qfMUz3zpvKjNV3HzLAVeFAxgHQpimjGLNvnsq++xQwWmBs2eGxMbvZYbta2dJmfn4PrJOsujOiH3DYv4jwNQpLTrgDNNOdgJ5Je172UhdRg+x+iEyj5dNeN+oVVd38cvQIIVdATofwwImPOD1VZtGyELCcSxyRUnvtbCK5KmIXQ0u4BLGcuR2P6An+XccDbNaq6LsRnupK0kb4YTAgvJ59kxWJcHcdZKp7FWsV6G7PDEFrc2xSgIS217zuUuSIcDKBUucSC9DI0U8UAJW/xbm9aEU1Ifp1TzppMhVKJ9tINH1rJtiUlxysI5SB0cbcUXGVMB4aGwWdO5hKANxIMcdk5RycyhbKMP94wy2If3LIyjrcby+k8lEK8YO1NV3B0N+KUcnzpnH+WDYzWSxapTiDgdkwirSP0TG1nllXd2GTnHmL5LlpGF+TJ/JBPzGaBhV2QEu0+eOTG3/DFwktfNBrIG7rrzkzssERHvBkzzJMxw0HRjyHnFcrHqGjuZKtDdEoP6UkgcvmZKUR5g+nVgz6DyY8caloZH3VvK1PS0+J+jWSNJ0tV4rt8tkqeVayHNhT9Htc1HMtYPy7nqRqHew0nGn+OsMq8of6Otg3SVdRHcK6Hw3hjBc057qH45L6tbCF6gGGRnB3YdxuG6IU9VUZfex8GI8jssTZksNyI1QwxABqjHoakYq8C9IeoZtEBfQPHd+u6wepj2RPF0qH/WavyIMeec2O5K3d7eYA6S0WsrSb/+/bp7RDQF/YwhU05mh2sJBrFb5oReopPYLmfgEIbV8Rkuuib28Fg=',
    'betfair_app_key_delayed': 'X0A25Ow6tURlp1y2hDLvVycKQ60U3cs6jywictm0OSDVu/lSFFc+mIg9Cuw4N6Wk',
    'betfair_app_key_live': 'ECAGuPs637oAyOXAsmWYSLS+xHFXfK4F+HhniPWcDjwtDv7x//9WUChXonOSnybA',
    'oneforge_api_key': 'TwDe09XqoMwZDCjEZxyl+8UG2xhPNkYAVuZVlMm8OZd61s6HuD7JRYvoz2yTe8rRV0dFQ+Hs9T6X5QM01WzfJU2mq1V9cKXaRCt9qvXH52Y=',
    'bitmex_api_secret': 'h5BqJ1TAklwjCiE2shdRNjtPbnJjVBsu/ZhxGLRR5rxhDzZnH2byRJzljqsRxogsBDvaqrtemaPuVU3eQ7sZVKCclkakPPx/YadvTpQ1bWg=',
    'bitmex_api_key': 'peKUMtOSb2EF8Wser/FNn1ni5X16xzXAPiw8Fh9bBJEd515TYOjPVuIMyuLkAUGT',
    'betegy_user': '86+iZGB/wlqhXgxbcM/eZpuTTpcsf4tn7AcGpr7nGLKWaf7t7dgum4r+sqnj/eIT',
    'betegy_pass': '4Mt2Q/JwNI8JsL/sguXyeEJXW4cmbjKEECDSHIblPBgycjm11f0XEhcpZRkzwcyj',
    # google_service_account_credentials is json.dumps of contents of google_service_account_key.json.
    # IMPORTANT: ['private_key'] which contains the ---BEGIN KEYFILE--- ... string must have actual newlines characters as line separators and not two characters of "\n" as in the json file. So make sure not to add it as a raw string.
    'google_service_account_credentials': 'oXLcex5Z5PEnMxV67Ud4MYrOMqiL7RkSqIwoTCj+GxJAiqouIjt0Zp2B0tGS8fYgcV+paTCAzw/vE0m+kL4edpTiLR7XVyDuCuGd7bFs116YsaBdIaLuUywJ0WC6NfSe2Q79hDJq/u88vKRhZjCPXBv+ymIVP8/rIWZ8zk2vAIFj2kLR/nxcbDR9Ue5JWHSOHH6856jqXmAXCgogu7Nm6ayiXp52YEdDzdyHZfwSFXOYDPw8rGYat1XtezSzRrWAWaCRJOp6s9InozzXKnFSFQlsCV06XuC7q7tbxRSTZziiwETBwpxBA01rXxFXPe5Oh5JGWTdXoQBPXnQ7jlJBzv4S14hgYU/edAU5LY4d6K2i5YAXxspdkGFyydw/v6trkYavS0jWJ2HeyixYEi8lnN58zTcpGaN+mALlhMvLc/PgpX5uX9r06UvGqQmk8fXhcG2B/PI6cmJNjnMLz3za0/GNYUY97UFpJuqUPjPoQ+gMoTqmtlI4zrvZFe3Cd37J41ROcPezHFTvTfGV2t7gxprTv+8O6HKCP56FZBUVPBxYSDFUWq669yyg9CIxh4FwiZ7grpxEiBbucZNtZIdyQTbctxGKs3sF9kOfE9pEzOLr2QUJnhvn+uXxQgYmoL2AtsDmfazctc87n20fwduSnOOhzuSNI8Nxyi7ulxo2WJqbkEnS7e6BKtIGm9nGjBl3o+Aj0FQE9whIawdEHckcx04ZFLKJt+Lur1B1izFXMPYz4qkuZblUATV+IwN4yJaI4cMJnKQoUj9Cy/vyizILdhj0PBxm9bmWmzP1ruoXw8lF7ALz1/v6I7AOZLPmObSIrlK4MfGe4O5092YIrlPVr+kuC9FNvszSwa5d5zEFPTJSNkx4t9AmZ34l/ve3YX8cAUz7KvOBQaWyjwkUwA3ZK4LTINxSNJihdeWPlWVELH66v+JjsZI0Go+ETN57bFIMceEV8n4oKTeJhjh5R8PP7Lg0q0w3BssTS0K3nzqwjIYkxt8r9Sx1fTVEuFCpB7iUqz+WVmhu4ukzyP/0qKneebeT+iuVP9VJvr2u/johimjfpAFHTTytbD9YCpaBDcQeyejIxhwPJfK7xeDeC5J6tbvC3DjJMKFP8hajgRMOtcAokVVOOHcdfSUEOL4cl1x67ScHgGMXAx1XZiLUW/ebuBmdSBVI845S5QlIcTeurLTjQ2KxyRnXAIyKNZ91N10ibvcFAmZBMrH/i5LpWThAaLba+WEdDjr/efjQHjmKbW0qweZdkAE0rSPthqEpw4/rgEpSLWCCqytP8CmkkubO05ny4zmLZ4DS5IXhcvSOE59tX1wkWGIgLH8NkJpaoQpuTMWHJPMLBL0hgF0pjYkI0nVD4wvhn9MhcOxW1zBAXj5hRh4eUcGCsR68LVKrDCi8zoVwhtVUkORhf8U/sryY0Q8FsNzOXIVMXmYKRQoRXEcXYhj8mEgOUT59/x3IT4VCRQ8Uqi5GdUg34vWCu1fgKOFBpANIuF2DZ5RXY6bNY1zqIb8XPjLE5EevwHzCNZxkrgKLVBaugpt2OJlPNVVXY3/663Fi7ho201zuoaSWx6MvzMsEFhtObXZRoivZpiHGy8CE+fkv97rKCylZv1Ewq4TiT+HO429aUXNx2Ympi0T9xmdRUwsX0Y1eToWIXst7nwbeit2VoJdySS4iS7KPMu05sfCL4Qi4DHC31wXcXsXbKUy4PsLWS3mxW9ZFSKYnuvvewoSnJDt6/+VbLvWmn6zC/yWLx8ZIANKG6gsx+tTqPJaIbH2M4zTvvzwhCxYd0Smn7m8RvbIpF4P7JIvJwkAAdrkm3P3bvazDT8fqtD4+2cjCiQiOafSIlErwOLebkJJa3jGEFkj9p4mFpi1S5M4Pls9zF7mY4hHla7FKGcQ4C2WTvD01vfBNm0gPtIv7laSSWzYOpbCTM7gTRd9vCUHYkBUZ1RKoOQROkKHJEiMKAo99LyYIMeOzgbmcIIZSysiVyAffsA8RQnbiQRb88x7I3FCWX9BhNBUzp8KXQt6hSfLjcvbc42eCT6jqOAB/cKm0nI1k0krn35Dn7lfcS1tzeH9EY877eUfc8g7hhz0XplvBPBx8uXqy6EJz1x4QqK1it8p9RBho2HkI3fbKFTJJW+UjgzgjAxQVm2s0QOHFmR2BrFXRLYM6iUIksVFwM9ZGRkmh8tGHkBlSHMMOxdAsf08hCG9RiSwqO7C6BYdjvb642UdzPI2nh+x6g+YtafS7Y4vS1x7ZEoFkkWjPS5yoMxSkypQ+Y5wReKwNSBH+XFFQBa780sh0q6k5UMMDlAkoL4WHqCyR7kVSbEBwedcaFA22pYfxjWPkjQ9Dz9UmA+eYZRW17pUo0bFBNxwj9HTuKLtrzPXr5VKpxshGIz0r4Is3z8zmiOGbqtPBYsdwyWYGVi9yu+OiwT1WH9AXTI1n7wmkRXOQTu5Pq4Kl/AXl3b3qHA3lgdDlPAB26m2bOWUY+BI4fRPAMN9I64F9aNf/okjRDZeDhcIF63NuAyqCBnSanxO00rD0E+Y/FPRTWXjmcnycIA4n9dBhaskD2dTUMBmYpVDBJd4e3/1SawDle3Rma77e6Qbfwcc6CjaLYvYISsyuiBlzWY7ULgQnHtFriBhVghDub+LkBYUjwJZ/vB0aiLFq0bj+G3RXJ4A2FUETslKMb9xcLSMPVCe6Hj1AW1V9vfEjWH1VUnpFIR+YxrouQTD5yFA3hlhIQ50Clhc7fbLBsb93uFCFQgalxo6CXxNlWnzHcDjoUn1caLzMP6Z3sCxp0EFDBFLvoUQuZwT1NZd8XAXZc9A11Dp4MC/kkcJ6lRVukQPrCrLONl3vbyIU6dKSpi0e8aXb4hnCsB+GrT/WSLqKF7l09LEr+CfZBgLxpDdG5GfEWZI0IQ9IGqwHMFvkICvJfLpleG9nUjL2EgedFB4dPKZygipKYVd3Go2gBuSgmtyprzIQ9hPBSHBmMyObcjY/kHg+Qt0LlG0PjTmqdsaraQ4eKpPdI0kPyfoUHthxMcKhYDqxxwKbv9irVzHTEXxTUW3DCAihbhv1M0xuaYW05n5KiPoQdzIYBJwh3/S2LLz9dyByM7HwYP3IlG6hMGpbNjZuNsnFC3JKlbLFA66ugb+Co0Rp',
    '1234': 'PXtL8Zl/FkRul8bWQzomgFq3hl/+714EtlEqM6AREMFgiukLTjAMSG79O5lW27iU', # should decrypt to 1234, used for testing password
}


# list of encoded keys that need json.loads
_encoded_values_jsoned = set(['google_service_account_credentials'])

try:
    from aes import aes_decrypt, aes_encrypt
except ModuleNotFoundError:
    from utils.aes import aes_decrypt, aes_encrypt

cache = {}
def cfg_get(key, _PASSWD = None):
    if key not in cache:
        if key in values:
            cache[key] = values[key]
        elif key in encoded_values:
            assert _PASSWD is not None
            if '1234' not in cache:
                cache['1234'] = aes_decrypt(encoded_values['1234'], _PASSWD)
                assert cache['1234'] == '1234', "Error: invalid password for decryption passed on env " + _PASSWD
            cache[key] = aes_decrypt(encoded_values[key], _PASSWD)
            if key in _encoded_values_jsoned:
                cache[key] = json.loads(cache[key])
        else:
            raise Exception('unknown cfg key: ' + key)
    return cache[key]


if __name__ == '__main__':
    import sys
    
    password = sys.argv[1]

    cfg_get('1234', password) # trigger testing of password


    print('encoding unencoded values:')
    for key, value in values.items():
        if key == 'addr_clients': continue
        if type(value) is not str: continue
        enc = str(aes_encrypt(value, password), 'utf-8')
        print("'" + key + "': '" + enc + "',")

    print('decoding encoded values:')
    for key, value in encoded_values.items():
        enc = aes_decrypt(value, password)
        print("'" + key + "': '" + enc + "'")
