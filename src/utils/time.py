"""
Converts between 3 representations:
 - integer which is the number of microsecs since UTC
 - str which is in format 2019-02-06 23:21:00.976
 - python datetime object

The 3 main functions that do the conversion are:
 to_int
 to_str
 to_datetime

These can take input in any format
"""

from datetime import datetime, timedelta
from time import time

def _str_to_datetime(str):
    try:
        return _datetime_to_naive(datetime.strptime(str, '%Y-%m-%d %H:%M:%S.%f'))
    except:
        pass
    try:
        return _datetime_to_naive(datetime.strptime(str, '%Y-%m-%d %H:%M:%S'))
    except:
        pass
    try:
        return _datetime_to_naive(datetime.strptime(str, '%Y-%m-%d'))
    except:
        pass
    assert False, str

def _str_to_timestamp(s):
    assert isinstance(s, str)
    return _datetime_to_timestamp(_str_to_datetime(s))

def _datetime_to_timestamp(dt):
    assert isinstance(dt, datetime)
    return int(round(dt.timestamp() * 1000.0))

def _timestamp_to_datetime(timestamp):
    return datetime.utcfromtimestamp(timestamp/1000.0)

def _datetime_to_str(dt):
    fmt = '%Y-%m-%d'
    if dt.minute + dt.hour + dt.second != 0: fmt += ' %H:%M:%S'
    if dt.microsecond != 0: fmt += '.%f'
    return dt.strftime(fmt)

def _timestamp_to_str(ts):
    return _datetime_to_str(_timestamp_to_datetime(ts))

_utc_tzinfo = None
def utc_tzinfo():
    global _utc_tzinfo
    if _utc_tzinfo is None:
        from datetime import tzinfo, timedelta
        ZERO = timedelta(0)
        class UTCtzinfo(tzinfo):
            def utcoffset(self, dt):
                return ZERO
            def tzname(self, dt):
                return "UTC"
            def dst(self, dt):
                return ZERO
        _utc_tzinfo = UTCtzinfo()
    return _utc_tzinfo

def _datetime_to_naive(v):
    assert isinstance(v, datetime), str(v) + ' has type ' + str(type(v)) + ' which is not datetime!'
    # if has timezone then convert to UTC and make it naive
    # https://stackoverflow.com/questions/5802108/how-to-check-if-a-datetime-object-is-localized-with-pytz
    if v.tzinfo is not None and v.tzinfo.utcoffset(v) is not None:
        v = v.astimezone(utc_tzinfo()).replace(tzinfo=None)
    return v

def to_int(v):
    """
    to_int is the main function which converts various datetime types into a UTC timestamp in microsecond precision integer
    suported types are:
        - int, long: assumed to be already UTC timestamp in microsecs, so just returns
        - string: useds dateutil.parser.parse and then voncerts to timestamp
        - datetime.datetime: if naive (no timezone) then just converts, if has timezone then first moves to UTC and then converts

    >>> to_int("2017-05-26 01:30:49")
    1495762249000L
    >>> to_int("2017-05-26 01:30:49.321")
    1495762249321L
    """
    if isinstance(v, int):
        assert v <= 4102444800000, "to_int assumes microsec UTC timestamps, you passed an integer that is greater than UTC(2100-01-01). Are you sure you didn't pass a nanosec timestamp? This can happen implicitly when calling to_list on pandas datetime series which uses np.datetime64 and silently converts to long nanosecs"
        return v
    elif isinstance(v, str):
        return _str_to_timestamp(v)
    elif isinstance(v, datetime):
        v = _datetime_to_naive(v)
        return _datetime_to_timestamp(v)
    else:
        raise Exception("Unknown type: " + type(v))

def to_str(v):
    """
    >>> to_str('1984-05-17 12:00:00')
    '1984-05-17 12:00:00'
    >>> to_str('1984-05-17 00:00:00')
    '1984-05-17'
    """
    return _timestamp_to_str(to_int(v))

def to_datetime(v):
    return _timestamp_to_datetime(to_int(v))

def weekday(t):
    t = to_datetime(t)
    return t.weekday()

def now():
    return _datetime_to_timestamp(datetime.utcnow())
